#include <stdlib.h>
#include <stdio.h>

int main(int argc, char *argv[]){
    if (argc < 2) {
        return 1;
    }
    else {
        int pedido = atoi(argv[1]);
        int trilegal, hidrogenio, helio, gravidade, gas;

        gas = pedido / 50;
        pedido = pedido%50;

        gravidade = pedido / 10;
        pedido = pedido - 10 * gravidade;

        helio = pedido / 5;
        pedido = pedido - 5 * helio;
	
	trilegal = pedido/3;
	pedido=pedido%3;

        hidrogenio = pedido;

        printf("%d hidrogenio\n", hidrogenio);
	printf("%d trilegal\n", trilegal);
        printf("%d helio\n", helio);
        printf("%d gravidade\n", gravidade);
        printf("%d gas\n", gas);

// huehue
        return 0;
    }
}
